# Deep Dive

A game written in Kotlin, using [libGDX](https://libgdx.com/) created during a 1 week Jam

The intention of the game is to navigate a diver far enough down, dodging fish that are swimming around.

You can run it locally using the Lwjgl3Launcher or build it into a HTML webapp using maven teaVm:build


# Sound effects

Sound effects from www.zapsplat.com
