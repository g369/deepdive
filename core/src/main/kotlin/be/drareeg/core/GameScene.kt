package be.drareeg.core

import be.drareeg.core.game.world.domain.*
import be.drareeg.core.game.world.render.WorldRenderModel
import be.drareeg.core.ui.FadingText
import be.drareeg.core.ui.TemporaryNess
import be.drareeg.core.ui.TemporaryUiElement
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.ScreenUtils
import kotlin.random.Random

interface LevelListener {
    fun onLevelWon()
    fun onLevelLost()
    fun onTreasureFound()
}

class GameScene(val mobileDevice: Boolean) : Scene, InputAdapter(), LevelListener {

    var currentLevel = 0

    //world is supposed to do all logic and ticking
    lateinit var world: World

    //the render model gets updated based on world events, but contains no logic
    lateinit var worldRenderModel: WorldRenderModel
    val temporaryUiElements = mutableListOf<TemporaryUiElement>()

    private val camera: OrthographicCamera = OrthographicCamera()
    private val batch = SpriteBatch()


    init {
        camera.setToOrtho(false, FRAMEWIDTH.toFloat(), FRAMEHEIGHT.toFloat())

        openNextLevel()
    }

    override fun setAsInputProcessor() {
        Gdx.input.inputProcessor = this
    }


    override fun keyDown(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.UP -> world.diver.goUp()
            Input.Keys.W -> world.diver.goUp()
            Input.Keys.DOWN -> world.diver.goDown()
            Input.Keys.S -> world.diver.goDown()
            Input.Keys.LEFT -> world.diver.goLeft()
            Input.Keys.A -> world.diver.goLeft()
            Input.Keys.RIGHT -> world.diver.goRight()
            Input.Keys.D -> world.diver.goRight()

            Input.Keys.ESCAPE -> SceneManager.pushScene(PauseMenuScene())


        }
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.UP -> world.diver.goingUp = false
            Input.Keys.W -> world.diver.goingUp = false
            Input.Keys.DOWN -> world.diver.goingDown = false
            Input.Keys.S -> world.diver.goingDown = false
            Input.Keys.LEFT -> world.diver.goingLeft = false
            Input.Keys.A -> world.diver.goingLeft = false
            Input.Keys.RIGHT -> world.diver.goingRight = false
            Input.Keys.D -> world.diver.goingRight = false
        }
        return false
    }

    private fun openNextLevel() {
        gameLost = false
        currentLevel++
        worldRenderModel = WorldRenderModel()
        world = buildWorldForLevel(currentLevel)



        if (currentLevel == 1) {
            temporaryUiElements.add(FadingText("Dive deep, watch your breath.", 2.0f, timeToLive = 5f))
        } else {
            temporaryUiElements.add(FadingText("Level $currentLevel", 4.0f))
        }
    }

    private fun buildWorldForLevel(currentLevel: Int): World {
        val levelFinishObjectY = -400f - 100f * currentLevel
        val world = World(worldRenderModel, levelFinishObjectY - 50, this)


        val endOfLevel = if (currentLevel == 7) {//TODO lvl 7
            SpawnTreasureChest(x = 200f, y = levelFinishObjectY)
        } else {
            SpawnPortal(x = 50f + Random.nextInt(0, FRAMEWIDTH - 100), y = levelFinishObjectY)
        }

        world.applyActions(
            listOf(
                SpawnDiver(x = 200, y = world.waterlevel.toInt()),
            ) +
                (0 until currentLevel * 3).map {
                    SpawnFish(
                        Vector3(50f + Random.nextFloat() * 300, 100f + Random.nextFloat() * levelFinishObjectY, 0f),
                        goingRight = Random.nextBoolean(),
                        randomFishSpeedFor(currentLevel)
                    )
                }
                + endOfLevel
        )
        return world
    }

    private fun randomFishSpeedFor(currentLevel: Int): Float {
        if (currentLevel < 3) {
            return Random.nextInt(300, 450).toFloat() / 100
        } else {
            return Random.nextInt(385, 580).toFloat() / 100

        }
    }


    fun onWearCrown() {
        world.wearCrown()
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun update(deltaTime: Float) {
    }


    override fun onLevelWon() {
        openNextLevel()
    }

    override fun onLevelLost() {
        gameLost = true
        timeSinceLost = 0f
    }

    override fun onTreasureFound() {
        SceneManager.pushScene(SelectTreasureScene(this))
    }

    var gameLost = false
    var timeSinceLost = 0f
    var touching = false
    var firstTouchPointX: Int = 0
    var firstTouchPointY: Int = 0

    override fun render(delta: Float) {
        if (touching && !Gdx.input.isTouched) {
            world.diver.stopMoving()
            worldRenderModel.removeJoyStick()
            touching = false
        }

        if (mobileDevice) { //this doesnt seem to work trough itchio with teavm
            mobileJoyStickDiverMovements()
        } else {
            mobileJoyStickDiverMovements()
//            mouseClickDiverMovements()
        }

        ScreenUtils.clear(0f, 0f, 0f, 1f)

        WalkmanGame.Ticker.doTicksFor(world, delta)
        if (gameLost) {
            timeSinceLost += delta
            if (timeSinceLost > 5f) {
                SceneManager.replaceScene(GameScene(mobileDevice))
            } else {
                actualRender(delta)
            }
            return
        }


        actualRender(delta)
    }

    private fun mouseClickDiverMovements() {
        if (Gdx.input.isTouched) {
            touching = true
            val cursorLocation = camera.unproject(
                Vector3(
                    Gdx.input.x.toFloat(),
                    Gdx.input.y.toFloat(),
                    0f
                )
            ) //unproject is van pixels naar viewport units (dus naar het 800,480 coord stelsel)

            val differenceWithDiverX = cursorLocation.x - world.diver.location.x - 25
            val differenceWithDiverY = cursorLocation.y - world.diver.location.y - WorldRenderModel.PUBLICACCESS_DISPLACEMENTVIEWPORT_Y

            world.diver.stopMoving()
            if (differenceWithDiverX > 3) {
                world.diver.goRight()
            }
            if (differenceWithDiverX < -3) {
                world.diver.goLeft()
            }
            if (differenceWithDiverY > 3) {
                world.diver.goUp()
            }
            if (differenceWithDiverY < -3) {
                world.diver.goDown()
            }
        }
    }

    private fun mobileJoyStickDiverMovements() {
        if (Gdx.input.isTouched) {
            if (!touching) {
                firstTouchPointX = Gdx.input.x
                firstTouchPointY = FRAMEHEIGHT - Gdx.input.y
                touching = true
                worldRenderModel.joyStickAt(firstTouchPointX, firstTouchPointY)
            }

            val xDifference = Gdx.input.x - firstTouchPointX
            val yDifference = (FRAMEHEIGHT - Gdx.input.y) - firstTouchPointY
            world.diver.stopMoving()
            if (xDifference > 10) {
                world.diver.goRight()
            }
            if (xDifference < -10) {
                world.diver.goLeft()
            }
            if (yDifference > 10) {
                world.diver.goUp()
            }
            if (yDifference < -10) {
                world.diver.goDown()
            }
        }
    }

    private fun actualRender(delta: Float) {
//       camera.update()
//        batch.projectionMatrix = camera.combined
        ScreenUtils.clear(0f, 0f, 0f, 1f)
        batch.begin()

        worldRenderModel.render(batch, delta)

        doTemporaryElements(delta, batch)

        batch.end()
    }

    private fun doTemporaryElements(delta: Float, batch: SpriteBatch) {
        temporaryUiElements.removeIf {
            it.render(delta, batch) == TemporaryNess.FINISHED
        }
    }


    //todo dispose

}


