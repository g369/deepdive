package be.drareeg.core

import be.drareeg.core.game.world.render.SoundEffects
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.ScreenUtils

class PauseMenuScene() : Scene {

    private val grassImg = Texture("water.png")
    private val batch = SpriteBatch()
    var mySkin = Skin(Gdx.files.internal("skins/clean-crispy-ui.json"))
    val resumeButton = TextButton("Dive Back In", mySkin)
    val muteButton = TextButton("Mute Sound", mySkin)


    val stage = Stage()

    init {
        resumeButton.x = 150f
        resumeButton.y = 100f
        resumeButton.width = 200f
        resumeButton.height = 200f
        resumeButton.color = Color(1f, 1f, 1f, 1f)
        resumeButton.addListener(
            object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    SceneManager.popScene()
                }
            })
        stage.addActor(resumeButton)


        muteButton.x = 150f
        muteButton.y = 450f
        muteButton.width = 200f
        muteButton.height = 200f
        muteButton.color = Color(1f, 1f, 1f, 1f)
        muteButton.addListener(
            object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    SoundEffects.toggle()
                    if(muteButton.text.toString() == "Mute Sound"){
                        muteButton.setText("Enable Sound")
                    }else{
                        muteButton.setText("Mute Sound")

                    }
                }
            })
        stage.addActor(muteButton)



        setAsInputProcessor()
    }

    override fun setAsInputProcessor() {
        Gdx.input.inputProcessor = stage
    }


    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun update(deltaTime: Float) {
    }


    override fun render(delta: Float) {
        //println("render PquseMenuScene")
        ScreenUtils.clear(0f, 0f, 0f, 1f)
        batch.begin()
        (0 until FRAMEWIDTH / 16).forEach { xRepeat ->
            (0 until FRAMEHEIGHT / 16).forEach { yRepeat ->
                batch.draw(grassImg, xRepeat * 16f, yRepeat * 16f)
            }
        }


//        stage.draw()
        resumeButton.draw(batch, 1f)
        muteButton.draw(batch, 1f)

        batch.end()
    }

}


