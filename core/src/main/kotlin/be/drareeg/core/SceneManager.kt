package be.drareeg.core

import java.util.*

interface Scene {
    fun onResume()
    fun onPause()
    fun update(deltaTime: Float)
    fun render(delta: Float)
    fun setAsInputProcessor()

}


class SceneManager {

    private val sceneStack: Stack<Scene> = Stack()

    companion object {
        val INSTANCE = SceneManager()

        fun pushScene(scene: Scene) {
            INSTANCE.pushScene(scene)
        }

        fun popScene() {
            INSTANCE.popScene()
        }

        fun replaceScene(scene: Scene) {
            INSTANCE.replaceScene(scene)
        }

        fun update(deltaTime: Float) {
            INSTANCE.update(deltaTime)
        }

        fun render(delta: Float) {
            INSTANCE.render(delta)
        }


    }

    private fun pushScene(scene: Scene) {
        if (sceneStack.isNotEmpty()) {
            sceneStack.peek().onPause()
        }
        sceneStack.push(scene)
        scene.setAsInputProcessor()
        scene.onResume()
    }

    private fun popScene() {
        if (sceneStack.isNotEmpty()) {
            sceneStack.pop().onPause()
        }
        if (sceneStack.isNotEmpty()) {
            sceneStack.peek().onResume()
            sceneStack.peek().setAsInputProcessor()
        }
    }

    private fun replaceScene(scene: Scene) {
        popScene()
        pushScene(scene)
        scene.setAsInputProcessor()
    }

    private fun update(deltaTime: Float) {
        sceneStack.peek().update(deltaTime)
    }

    private fun render(delta: Float) {
        sceneStack.peek().render(delta)
    }
}

