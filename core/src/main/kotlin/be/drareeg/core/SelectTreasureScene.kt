package be.drareeg.core

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextArea
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.ScreenUtils

class SelectTreasureScene(gameScene: GameScene) : Scene {//minder binnenpakken

    private val batch = SpriteBatch()
    var mySkin = Skin(Gdx.files.internal("skins/clean-crispy-ui.json"))
    val wearCrown = TextButton("Wear crown", mySkin)
    val ditchCrown = TextButton("Naah, I'm good", mySkin)
    val message = TextArea("You find a golden crown, it's breathtaking.", mySkin)

    val stage = Stage()

    init {
        wearCrown.x = 25f
        wearCrown.y = 100f
        wearCrown.width = 200f
        wearCrown.height = 200f
        wearCrown.color = Color(1f, 1f, 1f, 1f)
        wearCrown.addListener(
            object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    gameScene.onWearCrown()
                    SceneManager.popScene()
                }
            })
        stage.addActor(wearCrown)


        ditchCrown.x = 275f
        ditchCrown.y = 100f
        ditchCrown.width = 200f
        ditchCrown.height = 200f
        ditchCrown.color = Color(1f, 1f, 1f, 1f)
        ditchCrown.addListener(
            object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    gameScene.onLevelWon()
                    SceneManager.popScene()
                }
            })
        stage.addActor(ditchCrown)

        message.x = 50f
        message.y = 650f
        message.width = 400f
        message.height = 50f
        stage.addActor(message)

    }

    override fun render(delta: Float) {
        //println("render SelectTreasureScene")

        ScreenUtils.clear(1f, 1f, 0f, 1f)
        wearCrown.label.style.fontColor.a = 1f

        stage.viewport.apply()

        stage.draw()

        batch.begin()
        sprite.draw(batch)
        batch.end()
    }

    override fun setAsInputProcessor() {
        Gdx.input.inputProcessor = stage
    }


    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun update(deltaTime: Float) {
    }

    companion object {
        val diverImg = Texture("crown.png")
        val sprite = Sprite(diverImg)
        init {
            sprite.x = 180f
            sprite.y = 400f
        }
    }


}


