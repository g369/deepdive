package be.drareeg.core

import be.drareeg.core.game.world.domain.World
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx

//this is the siwe of the used screen in pixels
//nothing forces us to use these measurements in the World, even though we are doing that for now
const val FRAMEWIDTH = 480
const val FRAMEHEIGHT = 800

/** [com.badlogic.gdx.ApplicationListener] implementation shared by all platforms. */
class WalkmanGame(val mobileDevice: Boolean) : ApplicationAdapter() {


    override fun create() {
        // Gdx.audio.newMusic(Gdx.files.internal("relaxing.mp3")).play()
        SceneManager.pushScene(GameScene(mobileDevice))
//        SceneManager.pushScene(SelectTreasureScene(GameScene()))
    }

    class Ticker {

        companion object {
            val tickDurationInSeconds = 0.03f
            var tickProgression = 0f

            fun doTicksFor(world: World, delta: Float) {
                tickProgression += delta
                while (tickProgression > tickDurationInSeconds) {
                    world.tick()
                    tickProgression -= tickDurationInSeconds
                }
            }

        }
    }

    override fun render() {
        SceneManager.render(delta = Gdx.graphics.deltaTime)
    }
}
