package be.drareeg.core.game.world.domain

import com.badlogic.gdx.math.Vector3
import java.util.*

sealed interface DesiredAction

data class SpawnFish(
    val location: Vector3,
    val goingRight: Boolean,
    val speed: Float

) : DesiredAction {
}


data class MoveFish(
    val id: UUID,
    val xDelta: Int,
    val yDelta: Int
) : DesiredAction

data class MoveDiver(
    val id: UUID,
    val xDelta: Int,
    val yDelta: Int
) : DesiredAction


data class SpawnDiver(
    val id: UUID = UUID.randomUUID(),
    val x: Int,
    val y: Int
) : DesiredAction

data class SpawnPortal(
    val x: Float,
    val y: Float
) : DesiredAction

data class SpawnTreasureChest(
    val x: Float,
    val y: Float
) : DesiredAction


data class DiverInhales(
    val diverId: UUID
) : DesiredAction

data class DiverLosesSomeOxygen(
    val diverId: UUID
) : DesiredAction
