package be.drareeg.core.game.world.domain

import be.drareeg.core.FRAMEWIDTH
import com.badlogic.gdx.math.Vector3
import java.util.*

private val SPEED_VERTIVAL = 4//2


interface DiverVisionProvider {
    fun isBreathable(x: Float, y: Float): Boolean
}

data class Diver(val location: Vector3, val listener: RenderInformationListener, val waterLevel: Float, val id: UUID = UUID.randomUUID()) {

    val dead: Boolean
        get() {
            return oxygenPercentage <= 0f
        }
    var goingRight = false
    var goingLeft = false
    var goingUp = false
    var goingDown = false
    var oxygenPercentage = 100f

    init {
        listener.onDiverSpawn(location, oxygenPercentage)
    }

//todo eerste collision verlies je je vinnen of gastank

    fun desiredActionsOnTick(worldInfo: DiverVisionProvider): List<DesiredAction> {
        if (dead) {
            return emptyList()
        }

        val actions = mutableListOf<DesiredAction>()
        //todo keep diver within bounds
        val xDelta = if (goingRight) 4 else if (goingLeft) -4 else 0
        val yDelta = if (goingUp) SPEED_VERTIVAL else if (goingDown) -SPEED_VERTIVAL else 0

        if (xDelta != 0 || yDelta != 0) {
            actions += MoveDiver(id, xDelta, yDelta)
        }

        if (worldInfo.isBreathable(location.x, location.y)) {
            if (canStoreMoreOxygen()) {
                actions += DiverInhales(id)
            }
        } else {
            actions += DiverLosesSomeOxygen(id)
        }



        return actions
    }

    private fun canStoreMoreOxygen(): Boolean {
        return oxygenPercentage != 100f
    }

    fun move(action: MoveDiver) {
        location.x += action.xDelta
        location.y += action.yDelta

        location.y = location.y.coerceAtMost(waterLevel) // todo diver shouldnt know waterlevel in this manner
        location.x = location.x.coerceAtLeast(0f)
        location.x = location.x.coerceAtMost(FRAMEWIDTH-35f)
        //todo apply logic here to determin when to call diverstopped, and only call that once? seems reasonable

        listener.diverMoved(id, location, action.xDelta, action.yDelta)//TODO actual moved delta could be different
    }

    fun goUp() {
        goingUp = true
        goingDown = false
    }

    fun goDown() {
        goingUp = false
        goingDown = true
    }

    fun goLeft() {
        goingRight = false
        goingLeft = true
    }

    fun goRight() {
        goingRight = true
        goingLeft = false
    }

    fun stopMoving() {
        goingRight = false
        goingLeft = false
        goingUp = false
        goingDown = false
    }

    fun inhaleSomeAir() {
        val replenishThisAmount = (100f - oxygenPercentage).coerceAtMost(2.5f)

        if (replenishThisAmount > 0f) {
            oxygenPercentage += replenishThisAmount
            listener.diverBreathedIn(replenishThisAmount, oxygenPercentage)
        }
    }

    fun exhaleSomeAir(percentage: Float = 0.15f) {
        val loseThisAmount = oxygenPercentage.coerceAtMost(percentage)
        if (loseThisAmount > 0f) {
            oxygenPercentage -= loseThisAmount
            //zorgen dat fish hit ander event is voor andere visuals
            listener.diverBreathedOut(loseThisAmount, oxygenPercentage)
            if (dead) {
                listener.diverDied(id)
            }
        }
    }


}
