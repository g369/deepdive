package be.drareeg.core.game.world.domain

import be.drareeg.core.FRAMEWIDTH
import com.badlogic.gdx.math.Vector3
import java.util.*

data class Fish(val location: Vector3, val listener: RenderInformationListener, val id: UUID = UUID.randomUUID(), var goingRight: Boolean = false, val speed :Float = 4f) {

    init {
        listener.onFishSpawn(this)
    }

    fun desiredActionsOnTick(): List<DesiredAction> {
        changeDirectionIfAtEdge()
        val xDelta = if (goingRight) speed else -speed
        return listOf(MoveFish(id, xDelta.toInt(), 0))//todo float
    }

    private fun changeDirectionIfAtEdge() {
        if (goingRight && location.x > FRAMEWIDTH - 20) {
            goingRight = false
        }
        if (!goingRight && location.x < -40) {
            goingRight = true
        }
    }

    fun move(action: MoveFish) {
        location.x += action.xDelta
        location.y += action.yDelta
        listener.fishMoved(id, location, action.xDelta)
    }


}
