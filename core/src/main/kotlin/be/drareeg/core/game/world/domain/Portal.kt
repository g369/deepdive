package be.drareeg.core.game.world.domain

import java.util.*

data class Portal(val x: Float, val y: Float, val listener: RenderInformationListener, val id: UUID = UUID.randomUUID()) {

    init {
        listener.onPortalSpawn(x, y, id)
    }

}
