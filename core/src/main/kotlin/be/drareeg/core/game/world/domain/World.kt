package be.drareeg.core.game.world.domain

import be.drareeg.core.LevelListener
import be.drareeg.core.game.world.render.SoundEffects
import com.badlogic.gdx.math.Vector3
import java.util.*
import kotlin.math.sqrt

class World(val renderListener: RenderInformationListener, floorY: Float, val levelListener: LevelListener) : DiverVisionProvider {
    val fishs = mutableListOf<Fish>()
    val portals = mutableListOf<Portal>()
    val treasureChests = mutableListOf<TreasureChest>()

    lateinit var diver: Diver //TODO make thise things private and have controls go through World
    val waterlevel = 200f
    var gameStatus: GameStatus = GameStatus.PLAYING

    enum class GameStatus {
        PLAYING,
        LEVEL_WON,
        LOST,
    }

    init {
        renderListener.onWorldCreated(
            waterlevel = waterlevel,
            floorLevel = floorY
        )
    }

    fun tick() {
        //println("ticking world in state $gameStatus")
        when (gameStatus) {
            GameStatus.PLAYING -> {
                val actions = getDesiredActionsOfAllEntities()
                applyActions(actions)

                handleFishCollisions()
                handlePortalCollisions()
                handleTreasureCollisions()
            }

            GameStatus.LEVEL_WON -> {

            }

            GameStatus.LOST -> {
                val actions = getDesiredActionsOfAllEntities()
                applyActions(actions)
            }
        }
    }

    private fun handlePortalCollisions() {
        //this dumb, make better, also hitboxes
        portals.forEach {
            val distanceBetween = distanceBetween(it, diver.location)
            if (distanceBetween < 30f) {
                //println("diver hit portal ${UUID.randomUUID()}")
                gameStatus = GameStatus.LEVEL_WON
                levelListener.onLevelWon()
                SoundEffects.portal() //Please dont mix soundeffects in the world domain
            }
        }
    }

    private fun handleTreasureCollisions() {
        //this dumb, make better, also hitboxes
        treasureChests.forEach {
            val distanceBetween = distanceBetween(it, diver.location)
            if (distanceBetween < 30f) {
                //println("diver hit treasureChest ${UUID.randomUUID()}")
                gameStatus = GameStatus.LEVEL_WON
                levelListener.onTreasureFound()
            }
        }
    }


    var recentCollisions = mutableMapOf<UUID, Int>()

    private fun handleFishCollisions() {
        recentCollisions = recentCollisions
            .filter { it.value > 0 }
            .mapValues { it.value - 1 }.toMutableMap()

        //this dumb, make better, also hitboxes
        fishs
            .filter { !recentCollisions.containsKey(it.id) }
            .forEach {
            val distanceBetween = distanceBetween(it.location, diver.location)
            if (distanceBetween < 30f) {
                //println("diver hit fish ${UUID.randomUUID()}")
                recentCollisions[it.id] = 30
                diver.exhaleSomeAir(20f)
                if (diver.dead) {
                    gameStatus = GameStatus.LOST
                    levelListener.onLevelLost()
                }
                //make the fish swim away?
            }
        }
    }

    private fun distanceBetween(it: Vector3, location: Vector3): Float {
        return distanceBetween(it.x, it.y, location.x, location.y)
    }

    private fun distanceBetween(it: Portal, location: Vector3): Float {
        return distanceBetween(it.x, it.y, location.x, location.y)
    }

    private fun distanceBetween(it: TreasureChest, location: Vector3): Float {
        return distanceBetween(it.x, it.y, location.x, location.y)
    }

    private fun distanceBetween(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        val x_d: Float = x1 - x2
        val y_d: Float = y1 - y2
        return sqrt((x_d * x_d + y_d * y_d).toDouble()).toFloat()

    }


    private fun getDesiredActionsOfAllEntities(): List<DesiredAction> {
        return fishs.flatMap { it.desiredActionsOnTick() } +
            diver.desiredActionsOnTick(this)
    }


    fun applyActions(actions: List<DesiredAction>) {
        actions.forEach { action ->
            when (action) {
                is MoveFish -> fishs.first { it.id == action.id }.move(action)
                is SpawnFish -> {
                    fishs.add(Fish(action.location, renderListener, goingRight = action.goingRight, speed = action.speed))
                }

                is MoveDiver -> diver.move(action)
                is SpawnDiver -> diver = Diver(Vector3(action.x.toFloat(), action.y.toFloat(), 0f), renderListener, waterlevel)
                is SpawnPortal -> portals.add(Portal(action.x, action.y, renderListener))
                is DiverInhales -> diver.inhaleSomeAir()
                is DiverLosesSomeOxygen -> {
                    diver.exhaleSomeAir()
                    if (diver.dead) {
                        gameStatus = GameStatus.LOST
                        levelListener.onLevelLost()
                    }
                }

                is SpawnTreasureChest -> treasureChests.add(TreasureChest(action.x, action.y, renderListener))
            }
        }
    }

    override fun isBreathable(x: Float, y: Float): Boolean {
        return y > waterlevel - 10
    }

    fun wearCrown() {
        renderListener.crownWorn()
        diver.exhaleSomeAir(100f)
        gameStatus = GameStatus.LOST

    }
}

interface RenderInformationListener {

    fun onFishSpawn(fish: Fish)//dit hier moet geen gans object zijn
    fun fishMoved(id: UUID, location: Vector3, xDelta: Int)
    fun onDiverSpawn(diver: Vector3, oxygenPercentage: Float)
    fun diverMoved(id: UUID, location: Vector3, xDelta: Int, yDelta: Int)
    fun onWorldCreated(waterlevel: Float, floorLevel: Float)
    fun onPortalSpawn(x: Float, y: Float, id: UUID)
    fun diverBreathedIn(deltaPercentage: Float, newOxygenPercentage: Float)
    fun diverBreathedOut(deltaPercentage: Float, newOxygenPercentage: Float)
    fun diverDied(id: UUID)
    fun onTreasureChestSpawn(x: Float, y: Float, id: UUID)
    fun crownWorn()
    fun joyStickAt(x: Int, y: Int)

}
