package be.drareeg.core.game.world.render

.import

import be.drareeg.core.ui.TemporaryGameElement
import be.drareeg.core.ui.TemporaryNess

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import kotlin.random.Random

private const val displaceXEvery = 0.2f

class Bubble(val originalX: Float, val originalY: Float,  deltaPercentage: Float) : TemporaryGameElement {

    var timeAlive = 0f
    var xDisplacement = 0
    var timeSinceLastXDisplacement = -displaceXEvery
    val bubbleSprite = if (deltaPercentage > 5) bigsprite else sprite


    override fun render(delta: Float, batch: SpriteBatch, displaceMentOfViewportY: Float): TemporaryNess {

        timeAlive += delta
        timeSinceLastXDisplacement += delta

        if (timeSinceLastXDisplacement > displaceXEvery) {
            timeSinceLastXDisplacement - displaceXEvery
            xDisplacement += Random.nextInt(-2, 2)
        }
        bubbleSprite.x = originalX + xDisplacement

        val logicalY = originalY + timeAlive * 150
        bubbleSprite.y = logicalY + displaceMentOfViewportY
        bubbleSprite.draw(batch)
        return if (logicalY > 200) TemporaryNess.FINISHED else TemporaryNess.CONTINUE //nasty that waterlevel knowledge is duplicated here
    }

    companion object {
        val bubbleImage = Texture("smallbubble.png")
        val bigbubbleImage = Texture("bigbubble.png")
        val sprite = Sprite(bubbleImage)
        val bigsprite = Sprite(bigbubbleImage)
    }


}
