package be.drareeg.core.game.world.render

import com.badlogic.gdx.Gdx
import java.time.Duration
import java.time.Instant

class SoundEffects {


    companion object {

        private val fishHitSound by lazy { Gdx.audio.newSound(Gdx.files.internal("hitfish.mp3")) }
        private val deadSound by lazy { Gdx.audio.newSound(Gdx.files.internal("dead.mp3")) }
        private val breathInSound by lazy { Gdx.audio.newSound(Gdx.files.internal("breathin.mp3")) }
        private val portalSound by lazy { Gdx.audio.newSound(Gdx.files.internal("portal.mp3")) }

        var lastPlayedBreath = Instant.now().minusSeconds(3)
        var muted = false

        fun fishHit() {
            if (!muted) {
                fishHitSound.play()
            }
        }

        fun dead() {
            if (!muted) {
                deadSound.play()
            }
        }

        fun portal() {
            if (!muted) {
                portalSound.play()
            }
        }

        fun breathIn() {
            if (!muted) {
                val duration = Duration.between(lastPlayedBreath, Instant.now())
                val milliseconds = duration.toMillis()

                if (milliseconds > 700) {
                    breathInSound.play(0.3f)
                    lastPlayedBreath = Instant.now()
                }
            }
        }

        fun toggle() {
            muted = !muted
        }

    }


}
