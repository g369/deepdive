package be.drareeg.core.game.world.render

import be.drareeg.core.FRAMEWIDTH
import be.drareeg.core.game.world.domain.Fish
import be.drareeg.core.game.world.domain.RenderInformationListener
import be.drareeg.core.game.world.render.import.Bubble
import be.drareeg.core.ui.FadingText
import be.drareeg.core.ui.TemporaryGameElement
import be.drareeg.core.ui.TemporaryNess
import be.drareeg.core.ui.TemporaryUiElement
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import java.util.*
import kotlin.random.Random


class WorldRenderModel : RenderInformationListener {


    val fishs = mutableListOf<FishRenderModel>()
    val portals = mutableListOf<PortalRenderModel>()
    val treasures = mutableListOf<TreasureChestRenderModel>()
    var diver: DiverRenderModel? = null
    var hud: HudRenderModel? = null

    var displaceMentOfViewportY = 0f
    var desiredDisplaceMentOfViewportY = 0f
    var waterLevel: Float? = null
    var floorLevel: Float? = null
    private val waterImage = Texture("water.png")
    private val backgroundImage = Texture("background.png")
    private val floorImage = Texture("floor.png")
    val temporaryUiElements = mutableListOf<TemporaryUiElement>()
    val temporaryGameElements = mutableListOf<TemporaryGameElement>()
    var joyStickX: Int? = null
    var joyStickY: Int? = null

    companion object {
        var PUBLICACCESS_DISPLACEMENTVIEWPORT_Y = 0
        val joystickImage = Texture("joystick.png")
    }


    private fun doTemporaryElements(delta: Float, batch: SpriteBatch, displaceMentOfViewportY: Float) {
        temporaryGameElements.removeIf {
            it.render(delta, batch, displaceMentOfViewportY) == TemporaryNess.FINISHED
        }
        temporaryUiElements.removeIf {
            it.render(delta, batch) == TemporaryNess.FINISHED
        }
    }

    fun render(batch: SpriteBatch, delta: Float) {
        drawBackground(batch)
        if (displaceMentOfViewportY != desiredDisplaceMentOfViewportY) {
            val maxScrollChange = delta * 150
            val desiredScroll = desiredDisplaceMentOfViewportY - displaceMentOfViewportY
            val effictiveScroll = Math.max(Math.min(desiredScroll, maxScrollChange), maxScrollChange * -1)
            displaceMentOfViewportY += effictiveScroll
            PUBLICACCESS_DISPLACEMENTVIEWPORT_Y =
                displaceMentOfViewportY.toInt() //better mechanism to get this knowledge in gamescene for touch support should exist
        }
        fishs.forEach { it.drawTo(batch, delta, displaceMentOfViewportY) }
        portals.forEach { it.drawTo(batch, delta, displaceMentOfViewportY) }
        treasures.forEach { it.drawTo(batch, delta, displaceMentOfViewportY) }
        diver?.drawTo(batch, displaceMentOfViewportY)
        hud?.drawTo(batch)
        doTemporaryElements(delta, batch, displaceMentOfViewportY)
        if (joyStickX != null && joyStickY != null) {
            renderJoyStick(batch, joyStickX ?: 0, joyStickY ?: 0)
        }
    }


    private fun renderJoyStick(batch: SpriteBatch, x: Int, y: Int) {
        batch.draw(joystickImage, (x - joystickImage.width / 2).toFloat(), (y - joystickImage.height / 2).toFloat())
    }

    private fun drawBackground(batch: SpriteBatch) {
        val ySplitWaterAir = waterLevel!! + displaceMentOfViewportY
        val ySplitWaterFloor = floorLevel!! + displaceMentOfViewportY

        batch.draw(backgroundImage, 0f, ySplitWaterAir)
        drawWaterBelow(ySplitWaterAir, batch)
        drawFloorBelow(ySplitWaterFloor, batch)
    }

    private fun drawWaterBelow(ySplitWaterAir: Float, batch: SpriteBatch) {
        val original = batch.color.cpy()
        val highestYToDrawWater = ySplitWaterAir - 16
        var startDrawingAt = highestYToDrawWater
        while (startDrawingAt > 0) {
            startDrawingAt -= 16f
        }
        var yRepeat = startDrawingAt
        while (yRepeat <= highestYToDrawWater) {
            batch.setColor(0.9f, 0.9f, 0.9f, determinegradiant(yRepeat, ySplitWaterAir))
            (0 until FRAMEWIDTH / 16).forEach { xRepeat ->
                batch.draw(waterImage, xRepeat * 16f, yRepeat)
            }
            yRepeat += 16f
        }
        batch.color = original
    }

    private fun drawFloorBelow(ySplitWaterFloor: Float, batch: SpriteBatch) {
        batch.draw(floorImage, 0f, ySplitWaterFloor - floorImage.height)
    }

    private fun determinegradiant(yRepeat: Float, ySplitWaterAir: Float): Float {
        val depthOfWater = ySplitWaterAir - yRepeat
        val gradient = Math.min(0.6f, depthOfWater * 0.001f)
        return 1 - gradient

    }

    override fun onFishSpawn(fish: Fish) {
        fishs.add(fish.toRenderModel())
    }

    override fun fishMoved(id: UUID, location: Vector3, xDelta: Int) {
        val first = fishs.first { it.id == id }
        first.x = location.x
        first.y = location.y
        if (xDelta != 0) {
            first.lastMoveWasRight = xDelta > 0
        }
    }

    override fun onDiverSpawn(spawnLocation: Vector3, oxygenPercentage: Float) {
        diver = DiverRenderModel(spawnLocation.x, spawnLocation.y, oxygenPercentage = oxygenPercentage)
        hud = HudRenderModel(oxygenPercentage)
    }


    class HudRenderModel(var oxygenPercentage: Float) {
        var mySkin = Skin(Gdx.files.internal("skins/clean-crispy-ui.json"))
        fun drawTo(batch: SpriteBatch) {
            val progressBar = ProgressBar(0f, 100f, 1f, true, mySkin)
            progressBar.value = oxygenPercentage
            progressBar.x = 30f
            progressBar.y = 20f
            progressBar.draw(batch, 1f)
        }

    }

    override fun diverMoved(id: UUID, location: Vector3, xDelta: Int, yDelta: Int) {
        diver!!.x = location.x
        diver!!.y = location.y


        if (yDelta > 0) {
            diver!!.lastMoveWasDown = false
        }

        if (yDelta < 0) {
            diver!!.lastMoveWasDown = true
        }

        if (xDelta > 0) {
            diver!!.lastMoveWasRight = true
        }

        if (xDelta < 0) {
            diver!!.lastMoveWasRight = false
        }


        if (diver!!.lastMoveWasDown) {
            if (yOfDiverOnFrame() < 600) {
                desiredDisplaceMentOfViewportY = (600 - diver!!.y)
            }
        }


        if (!diver!!.lastMoveWasDown) {
            if (yOfDiverOnFrame() > 200) {
                desiredDisplaceMentOfViewportY = (600 - diver!!.y)
            }
        }


    }

    override fun onWorldCreated(waterlevel: Float, floorLevel: Float) {
        this.waterLevel = waterlevel
        this.floorLevel = floorLevel
    }

    override fun onPortalSpawn(x: Float, y: Float, id: UUID) {
        portals.add(PortalRenderModel(x = x, y = y, id = id))
    }


    override fun onTreasureChestSpawn(x: Float, y: Float, id: UUID) {
        treasures.add(TreasureChestRenderModel(x = x, y = y, id = id))
    }

    override fun crownWorn() {
        //  diver?.crownWearing = true
        temporaryUiElements.add(FadingText("As you put on the crown", 2.0f, timeToLive = 5f, y = 200f))
        temporaryUiElements.add(FadingText("You realise your mistake", 2.0f, timeToLive = 5f, y = 150f))
        temporaryUiElements.add(FadingText("It's Breathtaking", 2.0f, timeToLive = 5f, preWaitTime = 3f, y = 100f))
    }

    override fun joyStickAt(x: Int, y: Int) {
        joyStickX = x
        joyStickY = y
    }

    override fun diverBreathedIn(deltaPercentage: Float, newOxygenPercentage: Float) {
        diver?.oxygenPercentage = newOxygenPercentage
        hud?.oxygenPercentage = newOxygenPercentage
        if (deltaPercentage > 2) {
            SoundEffects.breathIn()
        }
    }


    override fun diverBreathedOut(deltaPercentage: Float, newOxygenPercentage: Float) {
        diver?.oxygenPercentage = newOxygenPercentage
        hud?.oxygenPercentage = newOxygenPercentage
        val muchAirLost = deltaPercentage > 5
        if (Random.nextFloat() > 0.8f || muchAirLost) { //nasty that we compare to 5?
            val mouthX = diver!!.x + if (diver!!.lastMoveWasRight) 40 else 0
            temporaryGameElements.add(Bubble(mouthX, diver!!.y + 5, deltaPercentage))
            if (muchAirLost) {
                SoundEffects.fishHit()
            }
        }
    }

    override fun diverDied(id: UUID) {
        diver?.die()
        SoundEffects.dead()
    }


    private fun yOfDiverOnFrame() = diver!!.y + displaceMentOfViewportY
    fun removeJoyStick() {
        joyStickX = null
        joyStickY = null
    }
}


private fun Fish.toRenderModel(): FishRenderModel {
    return FishRenderModel(id = this.id, x = this.location.x, y = this.location.y, this.goingRight)

}

data class DiverRenderModel(
    var x: Float, var y: Float, var lastMoveWasDown: Boolean = true,
    var oxygenPercentage: Float, var lastMoveWasRight: Boolean = true
) {

    var dead = false

    fun drawTo(batch: SpriteBatch, displaceMentOfViewportY: Float) {
        sprite.setFlip(!lastMoveWasRight, dead)
        sprite.x = x
        sprite.y = y + displaceMentOfViewportY - diverImg.height / 2
        sprite.draw(batch)
    }

    fun die() {
        dead = true
    }

    companion object {
        val diverImg = Texture("diver.png")
        val sprite = Sprite(diverImg)
    }
}

data class FishRenderModel(val id: UUID, var x: Float, var y: Float, var lastMoveWasRight: Boolean) {


    fun drawTo(batch: SpriteBatch, delta: Float, displaceMentOfViewportY: Float) {
        sprite.setFlip(lastMoveWasRight, false)
        sprite.x = x
        sprite.y = y + displaceMentOfViewportY
        sprite.draw(batch)
    }

    companion object {
        val fishImg = Texture("fish.png")
        val sprite = Sprite(fishImg)
    }
}


data class PortalRenderModel(val id: UUID, var x: Float, var y: Float) {

    fun drawTo(batch: SpriteBatch, delta: Float, displaceMentOfViewportY: Float) {
        batch.draw(portalImage, x, y + displaceMentOfViewportY)
    }


    companion object {
        val portalImage = Texture("portal.png")
    }

}


data class TreasureChestRenderModel(val id: UUID, var x: Float, var y: Float) {

    fun drawTo(batch: SpriteBatch, delta: Float, displaceMentOfViewportY: Float) {
        batch.draw(treasureChestImage, x, y + displaceMentOfViewportY)
    }


    companion object {
        val treasureChestImage = Texture("treasure.png")
    }

}



