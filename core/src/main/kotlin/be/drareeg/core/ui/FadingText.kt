package be.drareeg.core.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align

typealias Seconds = Float

data class FadingText(val text: String, val scale: Float, val timeToLive: Seconds = 2f, val y:Float = (Gdx.graphics.height) / 23f, val preWaitTime: Float = 0f) : TemporaryUiElement {

    var timeLived: Float = 0f
    val labelStyle: Label.LabelStyle = Label.LabelStyle()
    val label: Label

    init {
        labelStyle.font = BitmapFont()
        labelStyle.fontColor = Color.WHITE
        labelStyle.font.data.setScale(scale) // Adjust the scale to make the text larger

        label = Label(text, labelStyle)
        val labelWidht = 400f
        val labelHeight = 250f
        label.setSize(labelWidht, labelHeight)
        label.setAlignment(Align.center)
        label.setPosition((Gdx.graphics.width - labelWidht) / 2, y)

    }

    override fun render(delta: Float, batch: SpriteBatch): TemporaryNess {
        timeLived += delta
        if(timeLived < preWaitTime){
            return TemporaryNess.CONTINUE
        }


        //println("fading text existed for $timeLived")
        if (timeLived >= timeToLive + preWaitTime) {
            return TemporaryNess.FINISHED
        }

        val alpha = 1.0f - ((timeLived-preWaitTime) / timeToLive)

        labelStyle.fontColor.a = alpha

        label.draw(batch, 1f)

        //if not doing this, then the alpha leaks to the TextButtons labels  ..... :'(
        labelStyle.fontColor.a = 1f

        return TemporaryNess.CONTINUE
    }
}
