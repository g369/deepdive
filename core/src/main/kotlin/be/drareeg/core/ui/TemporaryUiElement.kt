package be.drareeg.core.ui

import com.badlogic.gdx.graphics.g2d.SpriteBatch


enum class TemporaryNess {
    CONTINUE,
    FINISHED
}
interface TemporaryUiElement {
    fun render(delta: Float, batch: SpriteBatch): TemporaryNess


}

interface TemporaryGameElement {
    fun render(delta: Float, batch: SpriteBatch, displaceMentOfViewportY: Float): TemporaryNess
}
