@file:JvmName("Lwjgl3Launcher")

package be.drareeg.core.lwjgl3

import be.drareeg.core.FRAMEHEIGHT
import be.drareeg.core.FRAMEWIDTH
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import be.drareeg.core.WalkmanGame

/** Launches the desktop (LWJGL3) application. */
fun main() {
    // This handles macOS support and helps on Windows.
    if (StartupHelper.startNewJvmIfRequired())
      return
    Lwjgl3Application(WalkmanGame(false), Lwjgl3ApplicationConfiguration().apply {
        setTitle("walkman")
        setWindowedMode(FRAMEWIDTH, FRAMEHEIGHT)
        setWindowIcon(*(arrayOf(128, 64, 32, 16).map { "libgdx$it.png" }.toTypedArray()))
    })
}
