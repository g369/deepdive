@file:JvmName("TeaVMLauncher")

package be.drareeg.core.teavm

import be.drareeg.core.FRAMEHEIGHT
import be.drareeg.core.FRAMEWIDTH
import com.github.xpenatan.gdx.backends.teavm.TeaApplicationConfiguration
import com.github.xpenatan.gdx.backends.teavm.TeaApplication
import be.drareeg.core.WalkmanGame

/** Launches the TeaVM/HTML application. */
fun main() {
    val config = TeaApplicationConfiguration("canvas").apply {
        width = FRAMEWIDTH
        height = FRAMEHEIGHT
    }
    TeaApplication(WalkmanGame(TeaApplication.isMobileDevice()), config)
}
